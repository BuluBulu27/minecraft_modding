<<<<<<< HEAD
**Edit a file, create a new file, and clone from Bitbucket in under 2 minutes**

When you're done, you can delete the content in this README and update the file with details for others getting started with your repository.

*We recommend that you open this README in another tab as you perform the tasks below. You can [watch our video](https://youtu.be/0ocf7u76WSo) for a full demo of all the steps in this tutorial. Open the video in a new tab to avoid leaving Bitbucket.*

---

## Edit a file

You’ll start by editing this README file to learn how to edit a file in Bitbucket.

1. Click **Source** on the left side.
2. Click the README.md link from the list of files.
3. Click the **Edit** button.
4. Delete the following text: *Delete this line to make a change to the README from Bitbucket.*
5. After making your change, click **Commit** and then **Commit** again in the dialog. The commit page will open and you’ll see the change you just made.
6. Go back to the **Source** page.

---

## Create a file

Next, you’ll add a new file to this repository.

1. Click the **New file** button at the top of the **Source** page.
2. Give the file a filename of **contributors.txt**.
3. Enter your name in the empty file space.
4. Click **Commit** and then **Commit** again in the dialog.
5. Go back to the **Source** page.

Before you move on, go ahead and explore the repository. You've already seen the **Source** page, but check out the **Commits**, **Branches**, and **Settings** pages.

---

## Clone a repository

Use these steps to clone from SourceTree, our client for using the repository command-line free. Cloning allows you to work on your files locally. If you don't yet have SourceTree, [download and install first](https://www.sourcetreeapp.com/). If you prefer to clone from the command line, see [Clone a repository](https://confluence.atlassian.com/x/4whODQ).

1. You’ll see the clone button under the **Source** heading. Click that button.
2. Now click **Check out in SourceTree**. You may need to create a SourceTree account or log in.
3. When you see the **Clone New** dialog in SourceTree, update the destination path and name if you’d like to and then click **Clone**.
4. Open the directory you just created to see your repository’s files.

Now that you're more familiar with your Bitbucket repository, go ahead and add a new file locally. You can [push your change back to Bitbucket with SourceTree](https://confluence.atlassian.com/x/iqyBMg), or you can [add, commit,](https://confluence.atlassian.com/x/8QhODQ) and [push from the command line](https://confluence.atlassian.com/x/NQ0zDQ).
=======
![XRay Logo](http://i.imgur.com/002LJvM.jpg)
# XRay Mod - [![Forge Downloads](http://cf.way2muchnoise.eu/advanced-xray.svg)](https://mods.curse.com/mc-mods/minecraft/256256-advanced-xray) [![Forge Available For](http://cf.way2muchnoise.eu/versions/advanced-xray.svg)](https://mods.curse.com/mc-mods/minecraft/256256-advanced-xray)
Minecraft Forge XRay mod

### Feature:
- Build on Forge!
- Simple and Clean UI For Adding, Deleting and Editing the blocks you want to see
    - RGB Based color selector for full colour range
    - Simple UI to add new blocks from a Searchable List layout
    - Easy Editing and Deleting of blocks
- Ore Dictionary Support

### How to use:
Open settings -> controls and edit the button to open the GUI and the button to toggle the Overlay on and off.
Then press your key to open the GUI to edit the ores on the list. Then press your key to turn on the Overlay

### Previews:
The [Imgur Album](http://imgur.com/a/23dX5)
![XRAY](http://i.imgur.com/N3KOEaE.png)

### Supports

|Minecraft Version   | Mod Version | Forge Version | Branch | Author
|---|---|---|---|---
|1.12.2 | [1.4.0](https://github.com/MichaelHillcox/XRay-Mod/releases/tag/1.12.2-v1.4.0) | 14.23.0.2491 | [/master](https://github.com/MichaelHillcox/XRay-Mod/tree/master) | [Michael Hillcox](https://github.com/MichaelHillcox)
|1.12.1 | [1.3.4](https://github.com/MichaelHillcox/XRay-Mod/releases/tag/1.12.1-v1.3.4) | 14.22.1.2478 | [/1.12.1](https://github.com/MichaelHillcox/XRay-Mod/tree/1.12.1) | [Michael Hillcox](https://github.com/MichaelHillcox)
|1.12 | [1.3.3](https://github.com/MichaelHillcox/XRay-Mod/releases/tag/1.12-v1.3.3) | 14.21.0.2334 | [/1.12.x](https://github.com/MichaelHillcox/XRay-Mod/tree/1.12.x) | [Michael Hillcox](https://github.com/MichaelHillcox)
|1.11.2 | [1.3.3 *limited support*](https://github.com/MichaelHillcox/XRay-Mod/releases/tag/1.11.2-v1.3.3) | 13.20.0.2224 | [/1.11.x](https://github.com/MichaelHillcox/XRay-Mod/tree/1.11.x) | [Michael Hillcox](https://github.com/MichaelHillcox)
|1.10.2 | [1.3.1 *limited support*](https://github.com/MichaelHillcox/XRay-Mod/releases/tag/1.10.2-v1.3.1) | 12.18.3.2185 | [/1.10.x](https://github.com/MichaelHillcox/XRay-Mod/tree/1.10.x) | [Michael Hillcox](https://github.com/MichaelHillcox)
|1.9.4 | [1.0.9 - *Discontinued*](https://github.com/MichaelHillcox/XRay-Mod/releases/tag/1.9.4-v1.0.9) | 12.17.0.2051 | [/1.9.4](https://github.com/MichaelHillcox/XRay-Mod/tree/1.9.4) | [BondarenkoArtur](https://github.com/BondarenkoArtur) & [Michael Hillcox](https://github.com/MichaelHillcox)
|1.8.9 | [1.1.0 - *Discontinued*](https://github.com/MichaelHillcox/XRay-Mod/releases/tag/1.8.9-v1.1.0) | 11.15.1.1722 | [/1.8.x](https://github.com/MichaelHillcox/XRay-Mod/tree/1.8.x) | [Michael Hillcox](https://github.com/MichaelHillcox) With help from  [BondarenkoArtur](https://github.com/BondarenkoArtur)
|1.7.10 | [1.0.1.75 - *Discontinued*](https://github.com/MichaelHillcox/XRay-Mod/releases/tag/1.0.1.75) | 10.13.2.1291 | [/1.7.10](https://github.com/MichaelHillcox/XRay-Mod/tree/1.7.10) | [mcd1992](https://github.com/mcd1992) ([mcd1992 / GitLab](https://gitlab.com/mcd1992)) & [Michael Hillcox](https://github.com/MichaelHillcox)
|1.6.4 | 1.0.0 - *Discontinued* | 9.11.1.953 |  [/1.6.4](https://github.com/MichaelHillcox/XRay-Mod/tree/1.6.4) | [mcd1992](https://github.com/mcd1992) ([mcd1992 / GitLab](https://gitlab.com/mcd1992))

- Discontinued
    - This means the mod will no longer be supported unless a very large bug is reported.
- Limited support
    - This means the mod will only receive bug fixes when ones are listed. I will no longer be back porting feature changes unless I get bored

> A big thanks CJB for his rendering system.
>>>>>>> 1a6f0462a438786111edff0d4da187b94585bfec
